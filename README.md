###To install git submodules:
{
	git submodule init
	git submodule update
}

###[SKIP] Reinstall submodules:
{
	git submodule deinit -f .
	git submodule update --init
}

###42FileChecker
{
	cd 42FileChecker
	sh 42FileChecker.sh
	Follow instruction 7)
}

###42_printf_unit_tests
{
	cd 42_printf_unit_tests/tests
	make test
}

###pft
{
	sed -i '' 's/LIBFTPRINTF_DIR=../LIBFTPRINTF_DIR=..\/ft_printf/g' pft/options-config.ini
	cd pft
	make
	./test		
}

###wgorold_test
{
	cd wgorold_test
	sh z_check.sh
	valgrind --leak-check=full ./run_test > z_result
	sh z_check.sh -d
}

###printf-unit-test
{
	cd printf-unit-test
	make f
	./run_test f
}

###test.c
{
	make -C ./ft_printf && gcc -Wall -Wextra test.c -I./ft_printf -L./ft_printf -lftprintf && ./a.out
	make fclean -C ./ft_printf && rm a.out
}
